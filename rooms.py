# Rooms

import curses, curses.panel

# Curses settings

stdscr = curses.initscr()
curses.start_color()
curses.curs_set(0)
curses.noecho()
stdscr.keypad(1)

curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)
max_x = stdscr.getmaxyx()[1] - 1
max_y = stdscr.getmaxyx()[0] - 1

# Game settings
inventory = []
gender = ''

# Game Intro

def opening_scene():

    # Room text/descirption 

    opening_text = """
###################################################################
# Welcome to 'The Cabin' a horror text-adventure by: Travis Dowd  #
###################################################################


Do you have what it takes to make it out alive? To this day no one
has. Be warned, once you decide there is no going back...
"""   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, opening_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Alright, let's do this!")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Screw that, I'm out of here!")
    window_four.keypad(1)

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                opening_room()
            else:
                pass
        elif key == 50:
            window_four.bkgd(' ', curses.color_pair(1))
            window_three.bkgd(' ', curses.color_pair(0))
            window_four.refresh()
            window_three.refresh()
            key = window_four.getch()
            if key == 10:
                running = False
                break 
            else:
                pass
        else:
            continue
             
# First Room                       

def opening_room():
    
    # Room test/descirption    
    
    opening_room_text = '''
You open the closet and see your old clothes which you haven't
worn in years. In fact the last time you saw them was right here
at the cabin 5 years ago wehen...

That is a tough time to remember.

"What am I doing here?" you ask yourself. "Why am I here after all
of these years?" YOu have many questions, all of which you will need
to figure out the answers to soon. But first I should probablt get
dressed.

You look through your clothes and see some outfits that bring back
a slew of memories, some good and some, well... Interesting.

You see two that look decently clean: a pair of baggy jeans with
an old band t-shirt, and you also see an oversized red t-shirt that
syas "Love & Peace" on the front along with a pair of black leggings.
'''

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, opening_room_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Baggy jeans and old band t-shirt")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Red shirt and black leggins")
    window_four.keypad(1)
     
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0))
            window_three.refresh() 
            window_four.refresh()
            gender = 'male'
            key = window_three.getch()
            if key == 10:
                gender = 'male'
                window_four.clear()
                room_three_m()
            else:
                gender = 'female'
                pass
        elif key == 50:
            window_four.bkgd(' ', curses.color_pair(1))
            window_three.bkgd(' ', curses.color_pair(0)) 
            window_three.refresh()
            window_four.refresh()
            gender = 'female'
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                room_three_f()
            else:
                pass
        else:
            continue
 

def room_three_m():
    
    # Room test/descirption    

    under_bed = '''
"Oh yeah! I forgot I used to keep that under this bed."

After getting both boots on you try out the flashlight.

"Damn! Out of batteries."

You look underneath the door and notice that it's pitch black outside of your
room. A quick look around the room and you don't see anything of use.
''' # Need to have player open door, only option

    path_m = '''
A flood of emotions hits you as you realize the smell of your girlfriend's
perfume is still all over this shirt. Whenever you left to come to the 
woods with your family she would spray your clothes with her perfume so you
wouldn't feel so far away from her.

"Me feet are freezing!" you shout as you look around for socks and some
shoes. "Where did I used to put my shoes? Oh yeah! Underneath the bed"
You lie down on the floor and see some old combat boots amd what looks
like a pair of long socks that you happened to throw underneath the bed.

You grab the socks and pull them out from underneath the bed and slip them
over your feet. Next you grab the boots and put them on. As you go to put the
second boot on you feel something inside of it. Immediately you pull your foot
out expecting a dead mouse or something. To your suprise an old flashlight
falls right out of the boots.
'''

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.clear()
    window.refresh()
    window.addstr(5, 5, path_m + under_bed)  
            

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Open the door.")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1)) 
            window_three.refresh() 
            key = window_three.getch()
            if key == 10:
                room_four()
            else:
                pass 
        else:
            continue
 

def room_three_f():
    
    # Room test/descirption    

    under_bed = '''
"Oh yeah! I forgot I used to keep that under this bed."

After getting both boots on you try out the flashlight.

"Damn! Out of batteries."

You look underneath the door and notice that it's pitch black outside of your
room. A quick look around the room and you don't see anything of use.
''' # Need to have player open door, only option
    
    path_f = '''
A flood of emotions hits you as you realize the smell of your boyfriend's
cologne is still all over this shirt. Whenever you left to come to the 
woods with your family he would spray your clothes with his cologne so you
wouldn't feel so far away from him.

"Me feet are freezing!" you shout as you look around for socks and some
shoes. "Where did I used to put my shoes? Oh yeah! Underneath the bed"
You lie down on the floor and see furry boots amd what looks like a pair
of long socks that you happened to throw underneath the bed.

You grab the socks and pull them out from underneath the bed and slip them
over your feet. Next you grab the boots and put them on. As you go to put the
second boot on you feel something inside of it. Immediately you pull your foot
out expecting a dead mouse or something. To your suprise an old flashlight
falls right out of the boots.
'''

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.clear()
    window.refresh()
    window.addstr(5, 5, path_f + under_bed)  
            

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Open the door.")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))   
            window_three.refresh() 
            key = window_three.getch()
            if key == 10:
                room_four()
            else:
                pass
        else:
            continue
 


def room_four():
    
    # Room test/descirption    

    path_4 = '''
As the door slowly creeks open you see a light comming from the bottom of the
stairs straight ahead of you. To your right there is a short wooden cabinet
with some drawers which is decorated with pictures of your family. To the left
you see the bathroom and another room with it's door shut.
'''

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, path_4)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Look through the cabinet")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Go into the bathroom")
    window_four.keypad(1)
 

    window_five = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + (test * 2) - 2, 2)
    window_five.addstr(1, 1, "Check out the closed door")
    window_five.keypad(1)


    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four)
    panel_five = curses.panel.new_panel(window_five)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))   
            window_four.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(0))
            window_three.refresh()
            window_four.refresh()
            window_five.refresh()
            key = window_three.getch()
            if key == 10:
                cabinet_room()
            else:
                pass
        elif key == 50:
            window_four.bkgd(' ', curses.color_pair(1))
            window_three.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(0))
            window_four.refresh()
            window_three.refresh()
            window_five.refresh() 
            key = window_four.getch()
            if key == 10:
                bath_room()
            else:
                pass
        elif key == 51:
            window_five.bkgd(' ', curses.color_pair(1))
            window_three.bkgd(' ', curses.color_pair(0))
            window_four.bkgd(' ', curses.color_pair(0))
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_five.getch()
            if key == 10:
                other_room()
            else:
                pass
        else:
            continue
 

def cabinet_room():

    # Room text/descirption 

    cabinet_text = '''
You turn to the right and open the cabinet. Inside you find:

- Batteries
- An old key

"Hmm... This stuff cound come in handy. Where should I go now? I haven't
been to the bathroom, downstairs or the other room yet?"
'''   
    inventory.append('key')
    inventory.append('batteries')
    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, cabinet_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Check out the bathroom")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Try the other room")
    window_four.keypad(1)
 
    window_five = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + (test * 2) - 2, 2)
    window_five.addstr(1, 1, "Go down the stairs")
    window_five.keypad(1)
   

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four)
    panel_five = curses.panel.new_panel(window_five)
    panel_two.bottom() 
 
    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(0))
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                bath_room()
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1))
            window_five.bkgd(' ', curses.color_pair(0))
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                other_room()
            else:
                pass
        elif key == 51:
            window_four.bkgd(' ', curses.color_pair(0))
            window_three.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(1))
            window_five.refresh()
            window_four.refresh()
            window_three.refresh()
            key = window_four.getch()
            if key == 10:
                window_five.clear()
                downstairs_room()
            else:
                pass
        else:
            continue
             

def bath_room():

    # Room text/descirption 

    bath_text = '''
You look around the bathroom but don't find anything useful.
'''   
    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, bath_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Check out the other room")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Try the cabinet")
    window_four.keypad(1)
 
    window_five = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + (test * 2) - 2, 2)
    window_five.addstr(1, 1, "Go down the stairs")
    window_five.keypad(1)
   

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four)
    panel_five = curses.panel.new_panel(window_five)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_five.bkgd(' ', curses.color_pair(0))  
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                other_room()
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1)) 
            window_five.bkgd(' ', curses.color_pair(0))  
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                cabinet_room()
            else:
                pass
        elif key == 51:
            window_four.bkgd(' ', curses.color_pair(0))
            window_three.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(1))  
            window_five.refresh()
            window_four.refresh()
            window_three.refresh()
            key = window_four.getch()
            if key == 10:
                window_five.clear()
                downstairs_room()
            else:
                pass
        else:
            continue
             

def other_room():

    # Room text/descirption 

    other_text = '''
Walking over to the other room seems to give you the chills with each step.
With each step you take you realize that the cabin is quite, infact the most
silent you have ever seen the cabin. Even though the cabin is getting to you,
you make it to the other room. You try opening the door, but it seems blocked
off from the other side.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, other_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Check out the bathroom")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Try the cabinet")
    window_four.keypad(1)
 
    window_five = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + (test * 2) - 2, 2)
    window_five.addstr(1, 1, "Go down the stairs")
    window_five.keypad(1)
   

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four)
    panel_five = curses.panel.new_panel(window_five)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_five.bkgd(' ', curses.color_pair(0))  
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                bath_room()
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1)) 
            window_five.bkgd(' ', curses.color_pair(0))  
            window_five.refresh()
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                cabinet_room()
            else:
                pass
        elif key == 51:
            window_four.bkgd(' ', curses.color_pair(0))
            window_three.bkgd(' ', curses.color_pair(0))
            window_five.bkgd(' ', curses.color_pair(1))  
            window_five.refresh()
            window_four.refresh()
            window_three.refresh()
            key = window_four.getch()
            if key == 10:
                window_five.clear()
                downstairs_room()
            else:
                pass
        else:
            continue
             

def downstairs_room():

    # Room text/descirption 

    downstairs_text = '''
As you walk down the stairs you hear the wood creek on everystep. The light
is on downstairs s though someone else has been here, although strangely it
feels colder down here than it did up in the room. To the left of the stairs
is the kitchen and to the right is the dinning room which is where the light'
is comming from.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, downstairs_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Go into the kichen")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Follow the light into the dining room")
    window_four.keypad(1)

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                kitchen_room()
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                dinning_room()
            else:
                pass
        else:
            continue


def kitchen_room():

    # Room text/descirption 

    kitchen_text = '''
You look aorung the kitchen to find any clues of what the hell is going on here.
Suprisingly you find some car keys on the counter.

"Hm... These are not mine."

The dinning room is across from you which has a door leading to the living room.
At the base of the stairs is a door which leads to the car port outside.
'''   

    inventory.append('car_keys')
    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, kitchen_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Go into the dinning room")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Check out the door")
    window_four.keypad(1)

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three)
    panel_four = curses.panel.new_panel(window_four) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                dinning_room()
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                port_room()
            else:
                pass
        else:
            continue


def dinning_room():

    # Room text/descirption 

    dinning_text = '''
The light you saw upstairs is comming from the dinning room, so you decide
to check it out. The lamp in the corner of the room is on, which is quite
strange. Yet again, this whole situation is strange.

"What\'s that?" you say aloud to yourself as you notice a piece of paper
hanging on the wallnext to the lamp. It reads:

==================================================================
|  Yes, you are not alone. You are probably wondering who I am   |
|  Well let's just put it this way, I was always jealous of your |
|  relationship and now you are MINE! Don't do anything stupid,  |
|  I am watching you....                                         |
==================================================================

"What the fuck? I need to get out of here!"

You rush to the living room door as it's your only way out.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, dinning_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Open the door")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
  

    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))   
            window_three.refresh() 
            key = window_three.getch()
            if key == 10:
                key_room()
            else:
                pass 
        else:
            continue


def port_room():

    # Room text/descirption 

    port_text = '''
The door is locked and it's so dark outside that you can't make anything out.
You press your eyes closer to the window and can see what looks like a truck
parked outside

"Who's truck is that?" You have never seen this truck before.

"BBBBBAAAAAANNNNGGGGGG!!!!!!" The door leading to the living room from inside
the dinning room just slammed shut. Seeing as this door is locked, it's the
only out.
'''   

    inventory.append('car_keys')
    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, port_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Go into the dinning room")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))   
            window_three.refresh()
            key = window_three.getch()
            if key == 10:
                dinning_room()
            else:
                pass
        else:
            continue



def key_room():

    # Room text/descirption 

    key_text = '''
You run towards the door and try opeing it.

"Shit! It's locked! Do I have the key?"
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, key_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Check pockets for key...")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
	
    running = True
    while running:
	key = window_two.getch()
	if key == 27 : # 10 = enter, 27 = esc
	    running = False
	    break 
	elif key == 49:  
	    window_three.bkgd(' ', curses.color_pair(1))   
	    window_three.refresh()
	    key = window_three.getch()
	    if key == 10: 
                if 'key' in inventory:
                    living_room() 
                    pass
                else:
                    death_room() 
                    pass
	    else:
		pass
	else:
	    continue


def living_room():

    # Room text/descirption 

    living_text = '''
"Get back here!" You hear from behind you as you scramble to find the key.
Luckily you find it quickly and rush through the door. As you turn around
and close the door you see the outline of a tall man approaching the door,
he raises his arm up in the air and you slam the door shut and lock it.

You hear an incredibly violent slam on the door. You can tell he is trying
to break down the door. In the panic you around the room and see a chair
that you can barricade yourself in here with, or you can rush to the front
door.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, living_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Barricade yourself in")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
    
    test = (max_y / 8) + 2
    
    window_four = curses.newwin((max_y / 8) - 1, max_x -2,((max_y / 3) * 2) + test, 2)
    window_four.addstr(1, 1, "Make a run for it")
    window_four.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_four = curses.panel.new_panel(window_four)
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
        
    running = True
    while running:
        key = window_two.getch()
        if key == 27 : # 10 = enter, 27 = esc
            running = False
            break 
        elif key == 49:  
            window_three.bkgd(' ', curses.color_pair(1))  
            window_four.bkgd(' ', curses.color_pair(0)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                end_room()    
            else:
                pass
        elif key == 50:  
            window_three.bkgd(' ', curses.color_pair(0))  
            window_four.bkgd(' ', curses.color_pair(1)) 
            window_three.refresh()
            window_four.refresh()
            key = window_three.getch()
            if key == 10:
                window_four.clear()
                death_room()
            else:
                pass
        else:
            continue



def death_room():

    inventory = ''
    gender = ''

    # Room text/descirption     

    death_text = '''
You died!
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, death_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Play again?")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
	
    running = True
    while running:
	key = window_two.getch()
	if key == 27 : # 10 = enter, 27 = esc
	    running = False
	    break 
	elif key == 49:  
	    window_three.bkgd(' ', curses.color_pair(1))   
	    window_three.refresh()
	    key = window_three.getch()
	    if key == 10:
                opening_scene()
	    else:
		pass
	else:
	    continue


def end_room():

    # Room text/descirption     

    end_text = '''
Quickly thinking on your feet you grab the chair besides you and slam the top
of it underneath the door handle. This gives you just enough time to run for
the truck.

"Wait, do I have the keys?"
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, end_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Check pockets for car keys")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
	
    running = True
    while running:
	key = window_two.getch()
	if key == 27 : # 10 = enter, 27 = esc
	    running = False
	    break 
	elif key == 49:  
	    window_three.bkgd(' ', curses.color_pair(1))   
	    window_three.refresh()
	    key = window_three.getch()
	    if key == 10: 
                if 'car_keys' in inventory:
                    truck_room() 
                    pass
                else:
                    death_room() 
                    pass
	    else:
		pass
	else:
	    continue


def truck_room():

    # Room text/descirption     

    truck_text = '''
You shuffle through your pockets and grab the car keys you found on the counter.
As you get into the truck you hear the dinning room door finally crashing down.

"Oh fuck!" You shout as you start up the truck and begin to drive away. Looking
back into the rear view mirror you see the man standing outside of the cabin
with a long knife in his hands. "Thank God!" You are relieved to get away from
that old cabin. As you are driving away you look into the passenger seat and noticed
a change of clothes you recognize.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, truck_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Go through the clothes")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
	
    running = True
    while running:
	key = window_two.getch()
	if key == 27 : # 10 = enter, 27 = esc
	    running = False
	    break 
	elif key == 49:  
	    window_three.bkgd(' ', curses.color_pair(1))   
	    window_three.refresh()
	    key = window_three.getch()
	    if key == 10: 
                final_room()
	    else:
		pass
	else:
	    continue



def final_room():
    
    inventory = []
    gender = ''

    # Room text/descirption     

    final_text = '''
Those clothes look similar, the ones your roommate always wears.

"Holy shit! I can't believe it was them!" 

Now you know the truth, but remeber that nobody has ever left the cabin.....

Alive.
'''   

    window = curses.newwin((max_y / 3) * 2, max_x, 1, 1)
    window.addstr(5, 5, final_text)  

    # Background of choice box
    
    window_two = curses.newwin((max_y / 3) + 2, max_x, ((max_y / 3) * 2) + 1, 1) 
    window_two.keypad(1)
    window_two.box()
 
    # Choices
    
    window_three = curses.newwin((max_y / 8) - 1, max_x - 2,((max_y / 3) * 2) + 2, 2)
    window_three.addstr(1, 1, "Play again?")
    window_three.bkgd(' ', curses.color_pair(1))
    window_three.keypad(1)
 
    # Panels/layers

    panel = curses.panel.new_panel(window)
    panel_two = curses.panel.new_panel(window_two)
    panel_three = curses.panel.new_panel(window_three) 
    panel_two.bottom() 

    curses.panel.update_panels()
    curses.doupdate()
	
    running = True
    while running:
	key = window_two.getch()
	if key == 27 : # 10 = enter, 27 = esc
	    running = False
	    break 
	elif key == 49:  
	    window_three.bkgd(' ', curses.color_pair(1))   
	    window_three.refresh()
	    key = window_three.getch()
	    if key == 10:
                opening_scene()
	    else:
		pass
	else:
	    continue


