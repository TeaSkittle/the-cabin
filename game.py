# Programmed by: Travis Dowd
# Date: 08/08/18
# Description: Making a text adventure

from sys import exit
import random
import time


#Controls & Basic info

prompt = '> '
inventory = []
gender = ''
failedcmd = "I do not understand, try typing something else."

def help():
    print """Try using keywords from the story like: 'closet',
'stairs', and 'door' to move to/use certain items in the room. If
you want to know what you are currently carrying type 'inventory'."""
          
def gameEnd():
    print"Thank you for playing"
    print "\nWould you like to play agin?"
    gameStart()

class gameDeath():
    def __init__(self):
        deathList = [
            'You Died!',
            'Told you that no one has ever made it out alive...',
            'Looks like that hurt, want me to call your mommy?',
            'Nice try chump.',
            'Feel free to try again, if you dare...'
        ]
        deathMessage = random.choice(deathList)
        print deathMessage
        print "\nWould you like to play agin?"
        gameStart()

#intro and start
        
class gameIntro():
    def __init__(self):
        print "\n\n"
        print """
###################################################################
Welcome to 'The Cabin' a horror text-adventure by: Travis Dowd
###################################################################
"""
        print "\n"
        print """
Do you have what it takes to come out of The Cabin alive? To this day
no one has. Be warned, once you decide there is no going back...

Would you like to begin?
"""


class gameStart():
    def __init__(self):
        start = raw_input(prompt)
        if start == 'yes' or start == 'y' or start == "Yes":
            room1()
        else:
            exit()

# Room Intros

def room3Intro():
        print "To the left of the stairs is the kitchen and to the right is"
        print "the dinning room, which is where the light is comming from."
        print "Where do you go?"

def clothesIntro():
    print"""
You open the closet and see your old clothes which you haven't
worn in years. In fact the last time your saw them was right here
at the cabin 5 years ago when...

That is a tough time to rememeber.

'What am I doing here?' you say aloud. 'Why am I here after all of
these years?' You have many questions, all of which you will need
to figure out the answers to soon. but first I should probably get
dressed.
You look through your clothes and see some outfits that bring back
a slew of memories, some good and some, well... Interesting.

You see two that look decently clean: a pair of baggy jeans with
an old band T-Shirt, and you also see an oversized red T-shirt that
says 'Love & Peace' on the front along with a pair of black leggings.

Which do you put on: The first or the second?
"""
    clothes()

def underBed():
    print"""
'Oh yeah! I forgot I used to keep that under this bed.'

After getting both boots on your try out the flashlight.

'Damn! Out of batteries.'

You look underneath the door and notice that it's pitch black outside your room.
A quick look around the room and you don't see anything of use. So you open the
door."""
    inventory.append('flashlight')
    room2Intro()
    room2()

def room2Intro():
    print"""
As the door slowly creeks open you see a light comming from the bottom of the
stairs straight ahead of you. To your right there is a short wooden cabinet
with some drawers which is decorated with pictures of your family. To the left
you see the bathroom and another room with it's door shut.

What do you do?

"""
      
# Rooms

class room1():
    def __init__(self):
        print """\n
You awake in a cabin, one you remember from your childhood.

'How did I get here?'

As you sit up in bed you loook around the room and see a closet and a doorway.
"""
        print "Type 'help' for commands"
        choosePath1()

class room2():
    def __init__(self):
        path = raw_input(prompt)
        if path == 'help' or path == 'Help':
            help()
            return room2()
        elif path == 'stairs' or path == 'downstairs' or path == 'staircase':
            print"""
As you walk down the stairs you hear the wood creek on every step. The light
is on downstairs as though someone else has been here, although strangely it
feels colder down here than it did up in the room."""
            room3Intro()
            room3()
        elif path == 'cabinet' or path == 'Cabinet':
            print"""
You turn to the right and open the cabinet. Inside you find:

- Batteries
- A key

'Hmm... This stuff should come in handy? Where should I go to now? I havent been
to the bathroom, downstairs or the other room just yet?'
"""
            inventory.append('key')
            inventory.append('batteries')
            return room2()
        elif path == 'bath' or path == 'bathroom' or path == 'restroom':
            print"""
You quickly look around the bathroom and don't find anything to interesting. You
open the medicine cabinet and even that is emepty. As you close the medicine
cabinet you look at yourself in the mirror. Something seems off...
"""
            time.sleep(6)
            raw_input(prompt)
            print "\n\nAAAAAAAAAHHHHHHHH!!!!!!!!!!!\n\n"
            print "You jump as you hear a loud crash come from downstairs."
            print "\nWhat do you do?"
            return room2()
        elif path == 'door' or path == 'room' or path == 'other' \
             or path == 'Door' or path == 'Room' or path == 'Other':
            print"""
Walking over to the other the room seems to give you the chills with each step.
With each step you take you realize that the cabin is quite, infact the most
silent tou have ever seen the cabin. Even though the cabin is getting to you,
you make it to other room.
"""
            for x in inventory:
                if 'key' in inventory:
                    print"\nWhere do you go?"
                    return room2()
                else:
                    print "The door is locked"
                    break
        elif path == 'inventory':
            print inventory
            return room2()
        else:
            print failedcmd
            return room2()


class room3():
    def __init__(self):
        path = raw_input(prompt)
        if path == 'help' or path == 'Help':
            help()
            return room3()
        elif path == 'Inventory' or path == 'inventory':
            print inventory
            return room3()
        elif path == 'kitchen' or path == 'Kitchen' or path == 'left' \
             or path == 'Left':
            print """
As you look around the kitchen everything seems fine, but you are still
confused as to why you are out here in the cabin in the first place. Even
worse it is more concerning as to why you are out here alone. The silence
is enough to drive someone to madness, and the confusion makes things even
worse.  I suppose I should either look around in the kitchen or go into the
dinning room.

What should I do?
"""
            while True:
                path1 = raw_input(prompt)
                if path1 == 'kitchen' or path1 == 'Kitchen' or path1 == 'look':
                    print """
You look around the kithcen to find any clues of what the hell is going here.
You do find some car keys on the counter.

'Hm... These are not mine.'

The dinning room is across from you which has a door leading to the living room.
At the bottom of the stairs is door which leads to the car port outside. What do
you do?
"""
                    inventory.append('car_keys')
                    path2 = raw_input(prompt)
                    if path2 == 'Dinning' or path2 == 'dinning':
                        room4()
                    elif path2 == 'door' or path2 == 'port' or path2 == 'car port':
                        print """
The door is locked and its so dark outside you that you can't make anything out.
You press your eyes closer to the window and can see what looks like a truck parked
outside.

'Who's truck is that?' You have never seen this truck.

'BBBAAAANNNGGGG!!!!' The door leading to the lving room form the dinning room just
slammed shut. Seeing as this door is locked, it's the only way out.

'Guess I should go into the dinning room...'
"""
                        path3 = raw_input(prompt)
                        room4()
                    elif path2 == 'help' or path2 == 'Help':
                        help()
                        return room3()
                    elif path2 == 'inventory' or path2 == 'Inventory':
                        print inventory
                        return room3()
                    else:
                        print failedcmd
                elif path1 == 'dinning' or path == 'Dining':
                    room4()
                    break
            return room3()
        elif path == 'Dinning' or path == 'dinning' or path == 'right' \
             or path == 'Right':
            room4()
        else:
            print failedcmd
            return room3()      
                
class room4():
    def __init__(self):
        print """
The light you saw from upstairs is comming frim the dinning room, so you decide
to check it out. The lamp in the corner of the room is on, which is quite strange.
Yet again, this whole situtaion is strange.

'What's that?' you say aloud to your self as you notice a piece of paper hanging
on the wall next to the lamp. It reads:\n
"""  
        print """\n\n
==============================================================
Yes, you are not alone. You are probably wondering who I am.
Well let's just put it this way, I was always jealous of your
partner and now you are mine! Don't do anything stupid, I am
watching you....
===============================================================

'What the fuck? I need to get out of here!'

You rush to the living room door, as it's your only way out. 
"""
        choosePath3()
class clothes():
    def __init__(self):
        genderChoice = raw_input(prompt)
        if genderChoice == 'Help' or genderChoice == 'help':
            help()
            return clothes()
        elif genderChoice == 'inventory':
            print inventory
            return clothes()
        elif genderChoice == 'first' or genderChoice == 'First':
            gender = 'male'
            choosePathm()
        elif genderChoice == 'second' or genderChoice == 'Second':
            gender = 'female'
            choosePathf()
        else:
            print failedcmd
            return clothes()

# Paths

class choosePath1():
    def __init__(self):
        path = raw_input(prompt)
        if path == 'help' or path == 'Help':
            help()
            return choosePath1()
        elif path == 'closet' or path == 'Closet':
            clothesIntro()
        elif path == 'doorway' or path == 'Doorway':
            print"""\n\n
You head towards the door tring to figure what all is going on.
A cold rush of air crawls up your spine as you realize you are only
in your underwear.

'I should probably see if any clothes are in the closet.'
"""
            clothesIntro()
        elif path == 'door' or path == 'Door':
            print "Try 'doorway' instead."
            return choosePath1()
        elif path == 'inventory':
            print "-Inventory is empty-"
            return choosePath1()
        else:
            print failedcmd
            return choosePath1()


def choosePathf():
    print"""
A flood of emotions hits you as you realize the smell of your boyfriend's
cologne is still all over this shirt. Whenever you left to come to the
woods with your family he would spray your clothes with his cologne so you
wouldn't feel so far away from him."

'My feet are freezing!' you shout as you look around for socks and some
shoes. 'Where did I used to put my shoes? Oh Yeah! Underneath the bed!'
You lie down on the floor and see furry boots and what looks like a pair
of long socks that you happened to throw underneath the bed.

You grab the socks and pull them out from underneath the bed and slip them
over your feet. Next you grab the boots and put them on. As you go to put the
second boot on you feel something inside of it. Imediately you pull your foot
out expecting a dead mouse or something. To your suprise an old flashlight
falls right out of the boots."""
    underBed()

def choosePathm():
    print """
A flood of emotions hits you as you realize the smell of your girlfriends's
perfume is still all over this shirt. Whenever you left to come to the
woods with your family she would spray your clothes with her perfume so you
wouldn't feel so far away from her."

'My feet are freezing!' you shout as you look around for socks and some
shoes. 'Where did I used to put my shoes? Oh Yeah! Underneath the bed!'
You lie down on the floor and see some old combat boots and what looks
like a pair of long socks that you happened to throw underneath the bed.

You grab the socks and pull them out from underneath the bed and slip them
over your feet. Next you grab the boots and put them on. As you go to put the
second boot on you feel something inside of it. Imediately you pull your foot
out expecting a dead mouse or something. To your suprise an old flashlight
falls right out of the boots."""
    underBed()

class choosePath3():
    def __init__(self):
        print """\n
You run towards the door and try opening it. 'Shit! it's locked!' Do I have the
key?
"""
        path = raw_input(prompt)
        if path == 'inventory' or path == 'Inventory':
            for x in inventory:
                if 'key' in inventory:
                    print """
'Get back here!' You here from behind you as you scramble to find the key.
Luckily you find it quickly and rush through the door. As you turn around
and close the door you see the outline of a tall man approaching the door,
he raises his arm up it the air and you slam the door shut and lock it.

You hear an incredibly violent slam on the door. You can tell he trying to
break the door down. In the panic you look around the room and see a chair
that you can barricade yourself in here with, or you can rusn to the front
door.
"""
                    choosePath4()
                    break
                else:
                    gameDeath()
                break
        elif path == 'help' or path == 'Help':
            help()
            return choosePath3()
        else:
            print failedcmd
            return choosePath3()
    
class choosePath4():
    def __init__(self):
        print "What do you do?"
        path = raw_input(prompt)
        if path == 'chair' or path == 'Chair':
            print """
Quickly thinking on your feet you grab the chair besides you and slam the top
of it underneath the door handle. This gives you just enough time to run for
the front door. In a rush you swing the front door open and head straight for
the truck.
"""
            for x in inventory:
                if 'car_keys' in inventory:
                    print"""
You shuffle through your pockets and grab the car keys you found on the counter.
As you get into the truck you hear the dinning room door finally crashing down.

'Oh fuck!' You shout as you start up the truck and begin to drive away. Looking
back into the rear view mirror you see the man standing outside of the cabin
with a long knife in his hands. 'Thank god!' YOu are realeved to get away from
that old cabin. As you are drving away you look into the passenger seat and notcied
a change of clothes, ones that you recognize.
"""
                    if gender == 'female':
                        print"""
Those clothes belong to Matt, the ones your boyfreind's roommate always wears.
'Holy shit! That was Matt?' Now you know the truth, but remember nobody has ever
left The Cabin.....Alive.
"""
                        gameEnd()
                    else:
                        print"""
Those clothes belong to Rachael, the ones your girlfreind's roommate always wears.
'Holy shit! That was Rachael?' Now you know the truth, but remember nobody has ever
left The Cabin.....Alive.
"""
                        gameEnd()
        elif path == 'door' or path == 'Door':
            print """"
As you are running for the front door the door to the dinning room gets
busted down. You turn to see the man charging at you, with a large kitchen
knife. Before you can blink your eyes, it's over
"""
            gameDeath()
            pass
        else:
            print "You better do something fast!"
            return choosePath4()
        
    
# Game Engine
                
gameIntro()
gameStart()
