# Text adventure engine
# Author: Travis Dowd
# Start Date: September 3, 2018

import curses
from rooms import *

def main():

    opening_scene()
    
    curses.endwin()

if __name__ == "__main__":
    main()
